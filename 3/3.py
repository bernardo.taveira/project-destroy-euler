num = 600851475143
prime = 2
i = 0
not_prime = 1

while num > 1:
    if ( num % prime == 0 ):
        print(prime)
        # Verify if it is prime
        i = prime // 2 + 1
        while i > 2:
            i -= 1
            not_prime = 0
            if ( prime % i == 0 ):
                not_prime = 1
            break
        if ( not_prime == 0 ):
            num = num // prime
    else:
        prime += 1
