def reverse(s): 
        return s[::-1]

def isPalindrome(s):
    rev = reverse(s)
    if ( s == rev ):
        return True
    else:
        return False

n1 = 999
n2 = 999
pal = 0
max_pal = 0

while n2 > 1:
    pal = n1*n2
    if ( pal > max_pal and isPalindrome(str(pal)) == 1 ):
        max_pal = pal
    if ( n1 > 2 ):
        n1 -= 1
    else:
        n2 -= 1
        n1 = n2

print(max_pal)
